# Discandddat Backend API

## Set up Docker

1. Install Docker
2. Run `docker-compose build`
3. Run `docker-comopse up`

### Userful Docker Commands

-   check current docker images `docker images`
-   delete docker images `docker rmi <image id>`
-   remove any stopped containers and all unused images `docker system prune -a`
-   force rebuild and up `docker-compose up --force-recreate --build`
-   or use `docker-compose build --no-cache`
-   go into docker container `docker-compose exec app bash`

## Project Specific Setups

-   Run all db related within container
-   install ts-node globally for typeorm script
    -   `npm install ts-node -g`
-   Generate migrations or other typeorm commands, use `--` to pass in args
    -   ie migration: `npm run typeorm migration:generate -- -n User`
-   Generate JWT Access and Refresh Token Secret
    -   `node`
    -   `require('crypto').randomBytes(64).toString('hex')`
-   Run Migration
    -   `npm run typeorm migration:run`
-   Seed
    -   `npm run seed`
-   Run Tests
    -   `npm run test` ( `-- <test name>` for single tests running)
