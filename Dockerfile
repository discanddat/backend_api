FROM node:13.5.0

WORKDIR /var/www/html

COPY package.json .

RUN npm install
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    git\
    ssh \
    && rm -rf /var/lib/apt/lists/*
# RUN npm ci --only=production

# Bundle app source
COPY . .

CMD [ "npm", "run", "start" ]