require('dotenv').config()

import 'reflect-metadata'
import { ApolloServer } from 'apollo-server-express'
import express from 'express'
import { createConnection } from 'typeorm'
import { createSchema } from './utils/createSchema'
import passport from 'passport'
import passportFacebook from 'passport-facebook'
import router from './routes'
import { facebookOptions, facebookCallback } from './modules/auth/fbAuth'
import passportGoogle from 'passport-google-oauth20'
import { googleOptions, googleCallback } from './modules/auth/googleAuth'

const FacebookStrategy = passportFacebook.Strategy
const GoogleStrategy = passportGoogle.Strategy

const startServer = async () => {
	await createConnection()

	const schema = await createSchema()
	const server = new ApolloServer({
		schema,
		context: ({ req, res }: any) => ({ req, res }),
	})

	const app = express()

	passport.use(new FacebookStrategy(facebookOptions, facebookCallback))
	passport.use(new GoogleStrategy(googleOptions, googleCallback))

	server.applyMiddleware({ app, path: process.env.SERVER_PATH })

	app.use('/', router)

	app.listen({ port: 8080 }, () => console.log(`🚀 Server ready`))
}

startServer()
