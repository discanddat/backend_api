import { ObjectType, Field } from 'type-graphql'

@ObjectType()
export default class JWT {
	@Field()
	access_token: string

	@Field()
	refresh_token: string

	@Field()
	token_type: string
}
