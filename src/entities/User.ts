import { ObjectType, Field, ID } from 'type-graphql'
import {
	Entity,
	BaseEntity,
	PrimaryGeneratedColumn,
	Column,
	CreateDateColumn,
	UpdateDateColumn,
} from 'typeorm'

export enum UserPosition {
	HANDLER = 'handler',
	CUTTER = 'cutter',
	HYBRID = 'hybrid',
}

export enum WfdfAccreditationLevel {
	ADVANCE = 'advance',
	STANDARD = 'standard',
	NOT_ACCREDITED = 'not accredited',
}

export enum JerseySize {
	XXS = 'XXS',
	XS = 'XS',
	S = 'S',
	M = 'M',
	L = 'L',
	XL = 'XL',
	XXL = 'XXL',
}

@ObjectType()
@Entity('Users')
export default class User extends BaseEntity {
	@Field(() => ID)
	@PrimaryGeneratedColumn()
	id: number

	@Field()
	@Column()
	first_name: string

	@Field()
	@Column()
	last_name: string

	@Field()
	@Column({ default: false })
	admin: boolean

	@Field()
	@Column({ unique: true })
	email: string

	@Column({ nullable: true })
	password: string

	@Field({ nullable: true })
	@Column({ nullable: true })
	DOB: Date

	@Field({ nullable: true })
	@Column({ nullable: true })
	height: number

	@Field({ nullable: true })
	@Column({ nullable: true })
	vegetarian: boolean

	@Field({ nullable: true })
	@Column({ nullable: true })
	allergies: string

	@Field({ nullable: true })
	@Column({ nullable: true, type: 'enum', enum: UserPosition })
	position: UserPosition

	@Field({ nullable: true })
	@Column({ nullable: true })
	wfdf_id: number

	@Field({ nullable: true })
	@Column({ nullable: true, type: 'enum', enum: WfdfAccreditationLevel })
	wfdf_accreditation_level: WfdfAccreditationLevel

	@Field({ nullable: true })
	@Column({ nullable: true, type: 'enum', enum: JerseySize })
	jersey_size: JerseySize

	@Field({ nullable: true })
	@Column({ nullable: true })
	id_type: string

	@Field({ nullable: true })
	@Column({ nullable: true })
	id_number: string

	@Field({ nullable: true })
	@Column({ nullable: true })
	id_expiry_date: Date

	@Field({ nullable: true })
	@Column({ nullable: true })
	id_issue_country: String

	@Field({ nullable: true })
	@Column({ nullable: true })
	legal_first_name: String

	@Field({ nullable: true })
	@Column({ nullable: true })
	current_location: String

	@Field({ nullable: true })
	@CreateDateColumn()
	created_at: Date

	@Field({ nullable: true })
	@UpdateDateColumn()
	updated_at: Date

	@Field({ nullable: true })
	@Column({ nullable: true })
	deleted_at: Date
}
