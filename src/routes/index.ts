import express from 'express'
import passport from 'passport'
import { createTokens } from '../modules/auth/resolvers/login.resolver'
import User from '../entities/User'

const router = express.Router()

router.get('/', (_req, res) => {
	res.send('More exciting things to come! Stay tuned!')
})

router.get('/auth/facebook', passport.authenticate('facebook'))

router.get(
	'/auth/facebook/callback',
	passport.authenticate('facebook', { session: false, scope: ['email'] }),
	async (req, res) => {
		const jwt = createTokens(req.user as User)
		res.send(jwt)
	},
)

router.get(
	'/auth/google',
	passport.authenticate('google', { scope: ['profile', 'email'] }),
)

router.get(
	'/auth/google/callback',
	passport.authenticate('google', { session: false }),
	async (req, res) => {
		const jwt = createTokens(req.user as User)
		res.send(jwt)
	},
)

export default router
