import { InputType, Field, Int, registerEnumType } from 'type-graphql'
import { IsEmail, MinLength } from 'class-validator'
import {
	UserPosition,
	WfdfAccreditationLevel,
	JerseySize,
} from '../../../entities/User'

registerEnumType(UserPosition, {
	name: 'UserPosition',
})
registerEnumType(WfdfAccreditationLevel, {
	name: 'WfdfAccreditationLevel',
})
registerEnumType(JerseySize, {
	name: 'JerseySize',
})

@InputType()
export class UserUpdateInput {
	@Field({ nullable: true })
	first_name: string

	@Field({ nullable: true })
	last_name: string

	@Field({ nullable: true })
	@IsEmail()
	email: string

	@Field({ nullable: true })
	@MinLength(8)
	password: string

	@Field(() => Int, { nullable: true })
	DOB: Date

	@Field(() => Int, { nullable: true })
	height: number

	@Field({ nullable: true })
	vegetarian: boolean

	@Field({ nullable: true })
	allergies: string

	@Field(() => UserPosition, { nullable: true })
	position: UserPosition

	@Field({ nullable: true })
	wfdf_id: number

	@Field(() => WfdfAccreditationLevel, { nullable: true })
	wfdf_accreditation_level: WfdfAccreditationLevel

	@Field(() => JerseySize, { nullable: true })
	jersey_size: JerseySize

	@Field({ nullable: true })
	id_type: string

	@Field({ nullable: true })
	id_number: string

	@Field({ nullable: true })
	id_expiry_date: Date

	@Field({ nullable: true })
	id_issue_country: String

	@Field({ nullable: true })
	legal_first_name: String

	@Field({ nullable: true })
	current_location: String
}
