import { Resolver, Arg, Query, Int, Mutation } from 'type-graphql'
import User from '../../../entities/User'
import { UserRegisterInput } from '../inputs/UserRegisterInput'
import { UserUpdateInput } from '../inputs/UserUpdateInput'
import bcrypt from 'bcryptjs'
import { UserNotFoundError } from '../../../errors/UserNotFoundError'

@Resolver()
export class UserResolver {
	@Query(() => [User])
	users() {
		return User.find({ where: { deleted_at: null } })
	}

	@Query(() => User)
	async user(@Arg('id', () => Int) id: number): Promise<User | undefined> {
		const user = await User.findOne({
			where: { id, deleted_at: null },
		})
		if (!user) {
			throw new UserNotFoundError()
		}
		return user
	}

	@Mutation(() => User)
	async registerUser(
		@Arg('data', () => UserRegisterInput) data: UserRegisterInput,
	): Promise<User> {
		data['password'] = await bcrypt.hash(String(data['password']), 10)
		return User.create(data).save()
	}

	@Mutation(() => Boolean)
	async updateUser(
		@Arg('id', () => Int) id: number,
		@Arg('data', () => UserUpdateInput) data: UserUpdateInput,
	): Promise<Boolean> {
		const user = await User.findOne({ where: { id: id, deleted_at: null } })
		if (!user) {
			throw new UserNotFoundError()
		}
		const updatedUser = await User.update({ id }, data)

		if (updatedUser) {
			return true
		}
		return false
	}

	@Mutation(() => Boolean)
	async deleteUser(@Arg('id', () => Int) id: number): Promise<Boolean> {
		const user = await User.findOne({ where: { id: id, deleted_at: null } })
		if (!user) {
			throw new UserNotFoundError()
		}
		const deletedUser = await User.update(
			{ id },
			{ deleted_at: new Date() },
		)
		if (deletedUser) {
			return true
		}
		return false
	}
}
