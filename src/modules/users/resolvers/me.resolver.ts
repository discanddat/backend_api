import { Resolver, Query, Ctx, UseMiddleware } from 'type-graphql'
import User from '../../../entities/User'
import { ExpressContext } from '../../../utils/ExpressContext'
import { isAuthenicated } from '../../../middlewares/isAuthenticated'

@Resolver()
export class MeResolver {
	@Query(() => User)
	@UseMiddleware(isAuthenicated)
	async me(@Ctx() ctx: ExpressContext) {
		return await User.findOne(ctx.req.userId)
	}
}
