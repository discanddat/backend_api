require('dotenv').config()

import User from '../../entities/User'

export const googleOptions = {
	clientID: process.env.GOOGLE_CLIENT_ID as string,
	clientSecret: process.env.GOOGLE_CLIENT_SECRET as string,
	callbackURL: process.env.GOOGLE_CALLBACK + '/auth/google/callback',
}

export const googleCallback = async (
	_accessToken: any,
	_refreshToken: any,
	profile: any,
	done: any,
) => {
	const { family_name, given_name, email } = profile._json
	let user = await User.findOne({ where: { email } })
	if (!user) {
		user = await User.create({
			first_name: given_name,
			last_name: family_name,
			email,
		}).save()
	}
	done(null, user)
}
