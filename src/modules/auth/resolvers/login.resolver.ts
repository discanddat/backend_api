require('dotenv').config()

import { AuthenticationError, ApolloError } from 'apollo-server-express'
import bcrypt from 'bcryptjs'
import { verify, sign } from 'jsonwebtoken'
import { Arg, Mutation, Resolver } from 'type-graphql'
import User from '../../../entities/User'
import { UserNotFoundError } from '../../../errors/UserNotFoundError'
import JWT from '../../../entities/JWT'

export const createTokens = (user: User) => {
	const accessToken = sign(
		{ userId: user.id },
		String(process.env.JWT_ACCESS_TOKEN_SECRET),
		{ expiresIn: '15mins' },
	)
	const refreshToken = sign(
		{ userId: user.id },
		String(process.env.JWT_REFRESH_TOKEN_SECRET),
		{ expiresIn: '7d' },
	)

	const jwt = new JWT()
	jwt.access_token = accessToken
	jwt.refresh_token = refreshToken
	jwt.token_type = 'bearer'

	return jwt
}

@Resolver()
export class LoginResolver {
	@Mutation(() => JWT)
	async login(
		@Arg('email') email: string,
		@Arg('password') password: string,
	) {
		const user = await User.findOne({
			where: { email, deleted_at: null },
		})
		if (!user) {
			throw new UserNotFoundError()
		}

		const validated = await bcrypt.compare(password, user.password)
		if (!validated) {
			throw new AuthenticationError('INCORRECT_PASSWORD')
		}

		return createTokens(user)
	}

	@Mutation(() => JWT)
	async refreshToken(@Arg('refresh_token') refreshToken: string) {
		let data

		try {
			data = verify(
				refreshToken,
				String(process.env.JWT_REFRESH_TOKEN_SECRET),
			) as any
		} catch (err) {
			return new ApolloError(err)
		}

		const user = await User.findOne(data.userId)

		return createTokens(user as User)
	}
}
