require('dotenv').config()

import User from '../../entities/User'

export const facebookOptions = {
	clientID: process.env.FACEBOOK_CLIENT_ID as string,
	clientSecret: process.env.FACEBOOK_CLIENT_SECRET as string,
	callbackURL: process.env.FACEBOOK_CALLBACK + '/auth/facebook/callback',
	profileFields: ['id', 'email', 'name'],
}

export const facebookCallback = async (
	_accessToken: any,
	_refreshToken: any,
	profile: any,
	done: any,
) => {
	const { first_name, last_name, email } = profile._json
	let user = await User.findOne({ where: { email } })
	if (!user) {
		user = await User.create({
			first_name,
			last_name,
			email,
		}).save()
	}
	done(null, user)
}
