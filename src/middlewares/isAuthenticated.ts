import { MiddlewareFn } from 'type-graphql'
import { ExpressContext } from '../utils/ExpressContext'
import { AuthenticationError } from 'apollo-server-express'
import { verify } from 'jsonwebtoken'

export const isAuthenicated: MiddlewareFn<ExpressContext> = async (
	{ context },
	next,
) => {
	if (!context.req.headers.authorization) {
		throw new AuthenticationError('NO_AUTH_HEADER')
	}
	const access_token = context.req.headers.authorization!.split(' ')[1]

	try {
		const data = verify(
			access_token,
			String(process.env.JWT_ACCESS_TOKEN_SECRET),
		) as any

		context.req.userId = data.userId
	} catch (err) {
		throw new AuthenticationError('INVALID_TOKEN')
	}

	await next()
}
