import { ApolloError } from 'apollo-server-express'

export class UserNotFoundError extends ApolloError {
	constructor() {
		super('USER_NOT_FOUND')

		Object.defineProperty(this, 'name', { value: 'UserNotFoundError' })
	}
}
