import faker from 'faker'
import { Connection } from 'typeorm'
import User from '../entities/User'
import { gCall } from './utils/gcall'
import { testConn } from './utils/testConn'

let conn: Connection

beforeAll(async () => {
	conn = await testConn()
})

afterAll(async () => {
	await conn.close()
})

describe('User related tests', () => {
	const data = {
		first_name: faker.name.firstName(),
		last_name: faker.name.lastName(),
		email: faker.internet.email(),
		password: faker.internet.password(),
	}

	let user: User

	it('register user test', async () => {
		const response = await gCall({
			source: `
				mutation registerUser($data: UserRegisterInput!) {
					registerUser(data: $data) {
						first_name
						last_name
						email
					}
				}
			`,
			variableValues: {
				data,
			},
		})
		expect(response).toMatchObject({
			data: {
				registerUser: {
					first_name: data.first_name,
					last_name: data.last_name,
					email: data.email,
				},
			},
		})
		const dbUser = await User.findOne({ where: { email: data.email } })
		expect(dbUser).toBeDefined()
	})

	it('view created user', async () => {
		user = (await User.findOne({
			where: { email: data.email },
		})) as User

		const response = await gCall({
			source: `
				query user($id: Int!) {
					user(id: $id) {
						first_name
						last_name
						email
					}
				}
			`,
			variableValues: {
				id: user!.id,
			},
		})
		expect(response).toMatchObject({
			data: {
				user: {
					first_name: user.first_name,
					last_name: user.last_name,
					email: user.email,
				},
			},
		})
	})

	it('view all users', async () => {
		const response = await gCall({
			source: `
				query {
					users {
						first_name
						last_name
						email
					}
				}
			`,
		})
		expect(response).toBeTruthy()
	})

	it('update user', async () => {
		await gCall({
			source: `
				mutation updateUser($id: Int!, $data: UserUpdateInput!) {
					updateUser(id: $id, data:$data)
				}
			`,
			variableValues: {
				id: user.id,
				data: { first_name: 'test' },
			},
		})

		const dbUser = await User.findOne({
			where: { email: user.email },
		})
		expect(dbUser!.first_name).toBe('test')
	})

	it('delete user', async () => {
		await gCall({
			source: `
				mutation deleteUser($id: Int!) {
					deleteUser(id: $id)
				}
			`,
			variableValues: {
				id: user.id,
			},
		})
		const dbUser = await User.findOne({
			where: { email: user.email },
		})
		expect(dbUser!.deleted_at).toBeTruthy
	})
})
