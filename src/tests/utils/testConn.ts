require('dotenv').config()
import { createConnection } from 'typeorm'

export const testConn = (drop: boolean = false) => {
	return createConnection({
		type: 'postgres',
		host: process.env.TEST_DB_HOST,
		port: 5432,
		username: process.env.TEST_DB_USERNAME,
		password: '',
		database: process.env.TEST_DB_NAME,
		synchronize: drop,
		dropSchema: drop,
		entities: [__dirname + '/../../entities/*.ts'],
	})
}
