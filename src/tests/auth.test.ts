import { Connection } from 'typeorm'
import { testConn } from './utils/testConn'
import faker from 'faker'
import User from '../entities/User'
import { gCall } from './utils/gcall'
import bcrypt from 'bcryptjs'

let conn: Connection

beforeAll(async () => {
	conn = await testConn()
})

afterAll(async () => {
	await conn.close()
})

describe('Authentication related tests', () => {
	const data = {
		first_name: faker.name.firstName(),
		last_name: faker.name.lastName(),
		email: faker.internet.email(),
		password: bcrypt.hashSync('password', 10),
	}

	let user: User
	let refreshToken: string
	let accessToken: string

	it('login test', async () => {
		//create user for login
		user = await User.create(data).save()

		const response = await gCall({
			source: `
				mutation login($email: String!, $password: String!) {
					login(email: $email, password: $password) {
						access_token
						refresh_token
						token_type
					}
				}
			`,
			variableValues: {
				email: user.email,
				password: 'password',
			},
		})
		refreshToken! = await response.data!.login.refresh_token
		accessToken! = await response.data!.login.access_token

		expect(response).toMatchObject({
			data: {
				login: {
					access_token: expect.any(String),
					refresh_token: expect.any(String),
					token_type: 'bearer',
				},
			},
		})
	})

	it('refresh token test', async () => {
		const response = await gCall({
			source: `
				mutation refreshToken($refresh_token: String!) {
					refreshToken(refresh_token: $refresh_token) {
                        access_token
                        refresh_token
                        token_type
					}
				}
			`,
			variableValues: {
				refresh_token: refreshToken,
			},
		})
		expect(response).toMatchObject({
			data: {
				refreshToken: {
					access_token: expect.any(String),
					refresh_token: expect.any(String),
					token_type: 'bearer',
				},
			},
		})
	})

	it('me test', async () => {
		const response = await gCall({
			source: `
				query {
					me {
						email
					}
				}
			`,
			authorization: 'Bearer ' + accessToken,
		})
		expect(response).toMatchObject({
			data: {
				me: {
					email: user!.email,
				},
			},
		})
	})
})
