import {MigrationInterface, QueryRunner} from "typeorm";

export class createUsersTable1588670056786 implements MigrationInterface {
    name = 'createUsersTable1588670056786'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TYPE "Users_position_enum" AS ENUM('handler', 'cutter', 'hybrid')`, undefined);
        await queryRunner.query(`CREATE TYPE "Users_wfdf_accreditation_level_enum" AS ENUM('advance', 'standard', 'not accredited')`, undefined);
        await queryRunner.query(`CREATE TYPE "Users_jersey_size_enum" AS ENUM('XXS', 'XS', 'S', 'M', 'L', 'XL', 'XXL')`, undefined);
        await queryRunner.query(`CREATE TABLE "Users" ("id" SERIAL NOT NULL, "first_name" character varying NOT NULL, "last_name" character varying NOT NULL, "admin" boolean NOT NULL DEFAULT false, "email" character varying NOT NULL, "password" character varying, "DOB" TIMESTAMP, "height" integer, "vegetarian" boolean, "allergies" character varying, "position" "Users_position_enum", "wfdf_id" integer, "wfdf_accreditation_level" "Users_wfdf_accreditation_level_enum", "jersey_size" "Users_jersey_size_enum", "id_type" character varying, "id_number" character varying, "id_expiry_date" TIMESTAMP, "id_issue_country" character varying, "legal_first_name" character varying, "current_location" character varying, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP, CONSTRAINT "UQ_3c3ab3f49a87e6ddb607f3c4945" UNIQUE ("email"), CONSTRAINT "PK_16d4f7d636df336db11d87413e3" PRIMARY KEY ("id"))`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE "Users"`, undefined);
        await queryRunner.query(`DROP TYPE "Users_jersey_size_enum"`, undefined);
        await queryRunner.query(`DROP TYPE "Users_wfdf_accreditation_level_enum"`, undefined);
        await queryRunner.query(`DROP TYPE "Users_position_enum"`, undefined);
    }

}
