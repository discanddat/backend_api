import Faker from 'faker'
import { define } from 'typeorm-seeding'
import User from '../../entities/User'
import bcrypt from 'bcryptjs'

define(User, (faker: typeof Faker) => {
	const first_name = faker.name.firstName()
	const last_name = faker.name.lastName()
	const email = faker.internet.email(first_name, last_name)
	const password = bcrypt.hashSync('password', 10)

	const user = new User()
	user.first_name = first_name
	user.last_name = last_name
	user.email = email
	user.password = password
	return user
})
