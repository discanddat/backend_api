import { Factory, Seeder } from 'typeorm-seeding'
import { Connection } from 'typeorm'
import User from '../../entities/User'

export default class CreateUsers implements Seeder {
	public async run(factory: Factory, _connection: Connection): Promise<any> {
		await factory(User)().seedMany(10)
	}
}
