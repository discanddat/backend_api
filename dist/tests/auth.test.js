"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const testConn_1 = require("./utils/testConn");
const faker_1 = __importDefault(require("faker"));
const User_1 = __importDefault(require("../entities/User"));
const gcall_1 = require("./utils/gcall");
const bcryptjs_1 = __importDefault(require("bcryptjs"));
let conn;
beforeAll(async () => {
    conn = await testConn_1.testConn();
});
afterAll(async () => {
    await conn.close();
});
describe('Authentication related tests', () => {
    const data = {
        first_name: faker_1.default.name.firstName(),
        last_name: faker_1.default.name.lastName(),
        email: faker_1.default.internet.email(),
        password: bcryptjs_1.default.hashSync('password', 10),
    };
    let user;
    let refreshToken;
    let accessToken;
    it('login test', async () => {
        user = await User_1.default.create(data).save();
        const response = await gcall_1.gCall({
            source: `
				mutation login($email: String!, $password: String!) {
					login(email: $email, password: $password) {
						access_token
						refresh_token
						token_type
					}
				}
			`,
            variableValues: {
                email: user.email,
                password: 'password',
            },
        });
        refreshToken = await response.data.login.refresh_token;
        accessToken = await response.data.login.access_token;
        expect(response).toMatchObject({
            data: {
                login: {
                    access_token: expect.any(String),
                    refresh_token: expect.any(String),
                    token_type: 'bearer',
                },
            },
        });
    });
    it('refresh token test', async () => {
        const response = await gcall_1.gCall({
            source: `
				mutation refreshToken($refresh_token: String!) {
					refreshToken(refresh_token: $refresh_token) {
                        access_token
                        refresh_token
                        token_type
					}
				}
			`,
            variableValues: {
                refresh_token: refreshToken,
            },
        });
        expect(response).toMatchObject({
            data: {
                refreshToken: {
                    access_token: expect.any(String),
                    refresh_token: expect.any(String),
                    token_type: 'bearer',
                },
            },
        });
    });
    it('me test', async () => {
        const response = await gcall_1.gCall({
            source: `
				query {
					me {
						email
					}
				}
			`,
            authorization: 'Bearer ' + accessToken,
        });
        expect(response).toMatchObject({
            data: {
                me: {
                    email: user.email,
                },
            },
        });
    });
});
//# sourceMappingURL=auth.test.js.map