"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const faker_1 = __importDefault(require("faker"));
const User_1 = __importDefault(require("../entities/User"));
const gcall_1 = require("./utils/gcall");
const testConn_1 = require("./utils/testConn");
let conn;
beforeAll(async () => {
    conn = await testConn_1.testConn();
});
afterAll(async () => {
    await conn.close();
});
describe('User related tests', () => {
    const data = {
        first_name: faker_1.default.name.firstName(),
        last_name: faker_1.default.name.lastName(),
        email: faker_1.default.internet.email(),
        password: faker_1.default.internet.password(),
    };
    let user;
    it('register user test', async () => {
        const response = await gcall_1.gCall({
            source: `
				mutation registerUser($data: UserRegisterInput!) {
					registerUser(data: $data) {
						first_name
						last_name
						email
					}
				}
			`,
            variableValues: {
                data,
            },
        });
        expect(response).toMatchObject({
            data: {
                registerUser: {
                    first_name: data.first_name,
                    last_name: data.last_name,
                    email: data.email,
                },
            },
        });
        const dbUser = await User_1.default.findOne({ where: { email: data.email } });
        expect(dbUser).toBeDefined();
    });
    it('view created user', async () => {
        user = (await User_1.default.findOne({
            where: { email: data.email },
        }));
        const response = await gcall_1.gCall({
            source: `
				query user($id: Int!) {
					user(id: $id) {
						first_name
						last_name
						email
					}
				}
			`,
            variableValues: {
                id: user.id,
            },
        });
        expect(response).toMatchObject({
            data: {
                user: {
                    first_name: user.first_name,
                    last_name: user.last_name,
                    email: user.email,
                },
            },
        });
    });
    it('view all users', async () => {
        const response = await gcall_1.gCall({
            source: `
				query {
					users {
						first_name
						last_name
						email
					}
				}
			`,
        });
        expect(response).toBeTruthy();
    });
    it('update user', async () => {
        await gcall_1.gCall({
            source: `
				mutation updateUser($id: Int!, $data: UserUpdateInput!) {
					updateUser(id: $id, data:$data)
				}
			`,
            variableValues: {
                id: user.id,
                data: { first_name: 'test' },
            },
        });
        const dbUser = await User_1.default.findOne({
            where: { email: user.email },
        });
        expect(dbUser.first_name).toBe('test');
    });
    it('delete user', async () => {
        await gcall_1.gCall({
            source: `
				mutation deleteUser($id: Int!) {
					deleteUser(id: $id)
				}
			`,
            variableValues: {
                id: user.id,
            },
        });
        const dbUser = await User_1.default.findOne({
            where: { email: user.email },
        });
        expect(dbUser.deleted_at).toBeTruthy;
    });
});
//# sourceMappingURL=user.test.js.map