"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
const typeorm_1 = require("typeorm");
exports.testConn = (drop = false) => {
    return typeorm_1.createConnection({
        type: 'postgres',
        host: process.env.TEST_DB_HOST,
        port: 5432,
        username: process.env.TEST_DB_USERNAME,
        password: '',
        database: process.env.TEST_DB_NAME,
        synchronize: drop,
        dropSchema: drop,
        entities: [__dirname + '/../../entities/*.ts'],
    });
};
//# sourceMappingURL=testConn.js.map