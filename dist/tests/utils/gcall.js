"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const createSchema_1 = require("../../utils/createSchema");
let schema;
exports.gCall = async ({ source, variableValues, userId, authorization, }) => {
    if (!schema) {
        schema = await createSchema_1.createSchema();
    }
    return graphql_1.graphql({
        schema,
        source,
        variableValues,
        contextValue: {
            req: {
                userId,
                headers: {
                    authorization,
                },
            },
            res: {},
        },
    });
};
//# sourceMappingURL=gcall.js.map