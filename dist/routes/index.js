"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const passport_1 = __importDefault(require("passport"));
const login_resolver_1 = require("../modules/auth/resolvers/login.resolver");
const router = express_1.default.Router();
router.get('/', (_req, res) => {
    res.send('More exciting things to come! Stay tuned!');
});
router.get('/auth/facebook', passport_1.default.authenticate('facebook'));
router.get('/auth/facebook/callback', passport_1.default.authenticate('facebook', { session: false, scope: ['email'] }), async (req, res) => {
    const jwt = login_resolver_1.createTokens(req.user);
    res.send(jwt);
});
router.get('/auth/google', passport_1.default.authenticate('google', { scope: ['profile', 'email'] }));
router.get('/auth/google/callback', passport_1.default.authenticate('google', { session: false }), async (req, res) => {
    const jwt = login_resolver_1.createTokens(req.user);
    res.send(jwt);
});
exports.default = router;
//# sourceMappingURL=index.js.map