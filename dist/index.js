"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
require("reflect-metadata");
const apollo_server_express_1 = require("apollo-server-express");
const express_1 = __importDefault(require("express"));
const typeorm_1 = require("typeorm");
const createSchema_1 = require("./utils/createSchema");
const passport_1 = __importDefault(require("passport"));
const passport_facebook_1 = __importDefault(require("passport-facebook"));
const routes_1 = __importDefault(require("./routes"));
const fbAuth_1 = require("./modules/auth/fbAuth");
const passport_google_oauth20_1 = __importDefault(require("passport-google-oauth20"));
const googleAuth_1 = require("./modules/auth/googleAuth");
const FacebookStrategy = passport_facebook_1.default.Strategy;
const GoogleStrategy = passport_google_oauth20_1.default.Strategy;
const startServer = async () => {
    await typeorm_1.createConnection();
    const schema = await createSchema_1.createSchema();
    const server = new apollo_server_express_1.ApolloServer({
        schema,
        context: ({ req, res }) => ({ req, res }),
    });
    const app = express_1.default();
    passport_1.default.use(new FacebookStrategy(fbAuth_1.facebookOptions, fbAuth_1.facebookCallback));
    passport_1.default.use(new GoogleStrategy(googleAuth_1.googleOptions, googleAuth_1.googleCallback));
    server.applyMiddleware({ app, path: process.env.SERVER_PATH });
    app.use('/', routes_1.default);
    app.listen({ port: 8080 }, () => console.log(`🚀 Server ready`));
};
startServer();
//# sourceMappingURL=index.js.map