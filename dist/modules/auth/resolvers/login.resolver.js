"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
const apollo_server_express_1 = require("apollo-server-express");
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const jsonwebtoken_1 = require("jsonwebtoken");
const type_graphql_1 = require("type-graphql");
const User_1 = __importDefault(require("../../../entities/User"));
const UserNotFoundError_1 = require("../../../errors/UserNotFoundError");
const JWT_1 = __importDefault(require("../../../entities/JWT"));
exports.createTokens = (user) => {
    const accessToken = jsonwebtoken_1.sign({ userId: user.id }, String(process.env.JWT_ACCESS_TOKEN_SECRET), { expiresIn: '15mins' });
    const refreshToken = jsonwebtoken_1.sign({ userId: user.id }, String(process.env.JWT_REFRESH_TOKEN_SECRET), { expiresIn: '7d' });
    const jwt = new JWT_1.default();
    jwt.access_token = accessToken;
    jwt.refresh_token = refreshToken;
    jwt.token_type = 'bearer';
    return jwt;
};
let LoginResolver = class LoginResolver {
    async login(email, password) {
        const user = await User_1.default.findOne({
            where: { email, deleted_at: null },
        });
        if (!user) {
            throw new UserNotFoundError_1.UserNotFoundError();
        }
        const validated = await bcryptjs_1.default.compare(password, user.password);
        if (!validated) {
            throw new apollo_server_express_1.AuthenticationError('INCORRECT_PASSWORD');
        }
        return exports.createTokens(user);
    }
    async refreshToken(refreshToken) {
        let data;
        try {
            data = jsonwebtoken_1.verify(refreshToken, String(process.env.JWT_REFRESH_TOKEN_SECRET));
        }
        catch (err) {
            return new apollo_server_express_1.ApolloError(err);
        }
        const user = await User_1.default.findOne(data.userId);
        return exports.createTokens(user);
    }
};
__decorate([
    type_graphql_1.Mutation(() => JWT_1.default),
    __param(0, type_graphql_1.Arg('email')),
    __param(1, type_graphql_1.Arg('password')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], LoginResolver.prototype, "login", null);
__decorate([
    type_graphql_1.Mutation(() => JWT_1.default),
    __param(0, type_graphql_1.Arg('refresh_token')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], LoginResolver.prototype, "refreshToken", null);
LoginResolver = __decorate([
    type_graphql_1.Resolver()
], LoginResolver);
exports.LoginResolver = LoginResolver;
//# sourceMappingURL=login.resolver.js.map