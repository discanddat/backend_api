"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
const User_1 = __importDefault(require("../../entities/User"));
exports.facebookOptions = {
    clientID: process.env.FACEBOOK_CLIENT_ID,
    clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
    callbackURL: process.env.FACEBOOK_CALLBACK + '/auth/facebook/callback',
    profileFields: ['id', 'email', 'name'],
};
exports.facebookCallback = async (_accessToken, _refreshToken, profile, done) => {
    const { first_name, last_name, email } = profile._json;
    let user = await User_1.default.findOne({ where: { email } });
    if (!user) {
        user = await User_1.default.create({
            first_name,
            last_name,
            email,
        }).save();
    }
    done(null, user);
};
//# sourceMappingURL=fbAuth.js.map