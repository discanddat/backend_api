"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
const User_1 = __importDefault(require("../../entities/User"));
exports.googleOptions = {
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.GOOGLE_CALLBACK + '/auth/google/callback',
};
exports.googleCallback = async (_accessToken, _refreshToken, profile, done) => {
    const { family_name, given_name, email } = profile._json;
    let user = await User_1.default.findOne({ where: { email } });
    if (!user) {
        user = await User_1.default.create({
            first_name: given_name,
            last_name: family_name,
            email,
        }).save();
    }
    done(null, user);
};
//# sourceMappingURL=googleAuth.js.map