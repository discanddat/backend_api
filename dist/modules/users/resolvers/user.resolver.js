"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const User_1 = __importDefault(require("../../../entities/User"));
const UserRegisterInput_1 = require("../inputs/UserRegisterInput");
const UserUpdateInput_1 = require("../inputs/UserUpdateInput");
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const UserNotFoundError_1 = require("../../../errors/UserNotFoundError");
let UserResolver = class UserResolver {
    users() {
        return User_1.default.find({ where: { deleted_at: null } });
    }
    async user(id) {
        const user = await User_1.default.findOne({
            where: { id, deleted_at: null },
        });
        if (!user) {
            throw new UserNotFoundError_1.UserNotFoundError();
        }
        return user;
    }
    async registerUser(data) {
        data['password'] = await bcryptjs_1.default.hash(String(data['password']), 10);
        return User_1.default.create(data).save();
    }
    async updateUser(id, data) {
        const user = await User_1.default.findOne({ where: { id: id, deleted_at: null } });
        if (!user) {
            throw new UserNotFoundError_1.UserNotFoundError();
        }
        const updatedUser = await User_1.default.update({ id }, data);
        if (updatedUser) {
            return true;
        }
        return false;
    }
    async deleteUser(id) {
        const user = await User_1.default.findOne({ where: { id: id, deleted_at: null } });
        if (!user) {
            throw new UserNotFoundError_1.UserNotFoundError();
        }
        const deletedUser = await User_1.default.update({ id }, { deleted_at: new Date() });
        if (deletedUser) {
            return true;
        }
        return false;
    }
};
__decorate([
    type_graphql_1.Query(() => [User_1.default]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], UserResolver.prototype, "users", null);
__decorate([
    type_graphql_1.Query(() => User_1.default),
    __param(0, type_graphql_1.Arg('id', () => type_graphql_1.Int)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "user", null);
__decorate([
    type_graphql_1.Mutation(() => User_1.default),
    __param(0, type_graphql_1.Arg('data', () => UserRegisterInput_1.UserRegisterInput)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [UserRegisterInput_1.UserRegisterInput]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "registerUser", null);
__decorate([
    type_graphql_1.Mutation(() => Boolean),
    __param(0, type_graphql_1.Arg('id', () => type_graphql_1.Int)),
    __param(1, type_graphql_1.Arg('data', () => UserUpdateInput_1.UserUpdateInput)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, UserUpdateInput_1.UserUpdateInput]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "updateUser", null);
__decorate([
    type_graphql_1.Mutation(() => Boolean),
    __param(0, type_graphql_1.Arg('id', () => type_graphql_1.Int)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "deleteUser", null);
UserResolver = __decorate([
    type_graphql_1.Resolver()
], UserResolver);
exports.UserResolver = UserResolver;
//# sourceMappingURL=user.resolver.js.map