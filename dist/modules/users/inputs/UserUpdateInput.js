"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const class_validator_1 = require("class-validator");
const User_1 = require("../../../entities/User");
type_graphql_1.registerEnumType(User_1.UserPosition, {
    name: 'UserPosition',
});
type_graphql_1.registerEnumType(User_1.WfdfAccreditationLevel, {
    name: 'WfdfAccreditationLevel',
});
type_graphql_1.registerEnumType(User_1.JerseySize, {
    name: 'JerseySize',
});
let UserUpdateInput = class UserUpdateInput {
};
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], UserUpdateInput.prototype, "first_name", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], UserUpdateInput.prototype, "last_name", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], UserUpdateInput.prototype, "email", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    class_validator_1.MinLength(8),
    __metadata("design:type", String)
], UserUpdateInput.prototype, "password", void 0);
__decorate([
    type_graphql_1.Field(() => type_graphql_1.Int, { nullable: true }),
    __metadata("design:type", Date)
], UserUpdateInput.prototype, "DOB", void 0);
__decorate([
    type_graphql_1.Field(() => type_graphql_1.Int, { nullable: true }),
    __metadata("design:type", Number)
], UserUpdateInput.prototype, "height", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", Boolean)
], UserUpdateInput.prototype, "vegetarian", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], UserUpdateInput.prototype, "allergies", void 0);
__decorate([
    type_graphql_1.Field(() => User_1.UserPosition, { nullable: true }),
    __metadata("design:type", String)
], UserUpdateInput.prototype, "position", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", Number)
], UserUpdateInput.prototype, "wfdf_id", void 0);
__decorate([
    type_graphql_1.Field(() => User_1.WfdfAccreditationLevel, { nullable: true }),
    __metadata("design:type", String)
], UserUpdateInput.prototype, "wfdf_accreditation_level", void 0);
__decorate([
    type_graphql_1.Field(() => User_1.JerseySize, { nullable: true }),
    __metadata("design:type", String)
], UserUpdateInput.prototype, "jersey_size", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], UserUpdateInput.prototype, "id_type", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], UserUpdateInput.prototype, "id_number", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", Date)
], UserUpdateInput.prototype, "id_expiry_date", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], UserUpdateInput.prototype, "id_issue_country", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], UserUpdateInput.prototype, "legal_first_name", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], UserUpdateInput.prototype, "current_location", void 0);
UserUpdateInput = __decorate([
    type_graphql_1.InputType()
], UserUpdateInput);
exports.UserUpdateInput = UserUpdateInput;
//# sourceMappingURL=UserUpdateInput.js.map