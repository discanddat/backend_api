"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
exports.createSchema = () => type_graphql_1.buildSchema({
    resolvers: [__dirname + '/../modules/**/resolvers/*.{js,ts}'],
});
//# sourceMappingURL=createSchema.js.map