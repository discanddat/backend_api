"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_express_1 = require("apollo-server-express");
const jsonwebtoken_1 = require("jsonwebtoken");
exports.isAuthenicated = async ({ context }, next) => {
    if (!context.req.headers.authorization) {
        throw new apollo_server_express_1.AuthenticationError('NO_AUTH_HEADER');
    }
    const access_token = context.req.headers.authorization.split(' ')[1];
    try {
        const data = jsonwebtoken_1.verify(access_token, String(process.env.JWT_ACCESS_TOKEN_SECRET));
        context.req.userId = data.userId;
    }
    catch (err) {
        throw new apollo_server_express_1.AuthenticationError('INVALID_TOKEN');
    }
    await next();
};
//# sourceMappingURL=isAuthenticated.js.map