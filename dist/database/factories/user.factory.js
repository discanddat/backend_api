"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_seeding_1 = require("typeorm-seeding");
const User_1 = __importDefault(require("../../entities/User"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
typeorm_seeding_1.define(User_1.default, (faker) => {
    const first_name = faker.name.firstName();
    const last_name = faker.name.lastName();
    const email = faker.internet.email(first_name, last_name);
    const password = bcryptjs_1.default.hashSync('password', 10);
    const user = new User_1.default();
    user.first_name = first_name;
    user.last_name = last_name;
    user.email = email;
    user.password = password;
    return user;
});
//# sourceMappingURL=user.factory.js.map