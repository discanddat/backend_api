"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = __importDefault(require("../../entities/User"));
class CreateUsers {
    async run(factory, _connection) {
        await factory(User_1.default)().seedMany(10);
    }
}
exports.default = CreateUsers;
//# sourceMappingURL=user.seed.js.map