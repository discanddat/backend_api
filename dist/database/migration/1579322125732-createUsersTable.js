"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class createUsersTable1579322125732 {
    constructor() {
        this.name = 'createUsersTable1579322125732';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "Users" ("id" SERIAL NOT NULL, "first_name" character varying NOT NULL, "last_name" character varying NOT NULL, "email" character varying NOT NULL, "password" character varying, "age" integer, "height" integer, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP, CONSTRAINT "UQ_3c3ab3f49a87e6ddb607f3c4945" UNIQUE ("email"), CONSTRAINT "PK_16d4f7d636df336db11d87413e3" PRIMARY KEY ("id"))`, undefined);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "Users"`, undefined);
    }
}
exports.createUsersTable1579322125732 = createUsersTable1579322125732;
//# sourceMappingURL=1579322125732-createUsersTable.js.map