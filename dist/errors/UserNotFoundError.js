"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_express_1 = require("apollo-server-express");
class UserNotFoundError extends apollo_server_express_1.ApolloError {
    constructor() {
        super('USER_NOT_FOUND');
        Object.defineProperty(this, 'name', { value: 'UserNotFoundError' });
    }
}
exports.UserNotFoundError = UserNotFoundError;
//# sourceMappingURL=UserNotFoundError.js.map