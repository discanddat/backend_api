require('dotenv').config()

const rootDir = process.env.NODE_ENV === 'development' ? 'src' : 'dist'

module.exports = {
	type: process.env.DB_TYPE,
	host: process.env.DB_HOST,
	port: process.env.DB_PORT,
	username: process.env.DB_USERNAME,
	password: process.env.DB_PASSWORD,
	database: process.env.DB_NAME,
	logging: false,
	synchronize: false,
	entities: [rootDir + '/entities/**/*.{js,ts}'],
	migrations: [rootDir + '/database/migration/**/*.{js,ts}'],
	subscribers: [rootDir + '/subscriber/**/*.{js,ts}'],
	seeds: [rootDir + '/database/seeds/**/*.seed.{js,ts}'],
	factories: [rootDir + '/database/factories/**/*.factory.{js,ts}'],
	cli: {
		entitiesDir: 'src/entities',
		migrationsDir: 'src/database/migration',
		subscribersDir: 'src/subscriber',
	},
}
